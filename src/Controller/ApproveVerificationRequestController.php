<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\VerificationRequest;

class ApproveVerificationRequestController
{
    public function __invoke(VerificationRequest $data)
    {
        if($data->getStatus()!=VerificationRequest::STATUS_VERIFICATION_APPROVED){
            $data->setStatus(VerificationRequest::STATUS_VERIFICATION_APPROVED);
            
            $user = $data->getUser();
            $roles = $user->getRoles();
            $roles[] = "BLOGGER";
            $user->setRoles($roles);

        }
        return $data;
    }
 
}
