<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\VerificationRequest;

class RejectVerificationRequestController
{
    public function __invoke(VerificationRequest $data)
    {
        $data->setStatus(VerificationRequest::STATUS_VERIFICATION_DECLINED);
        return $data;
    }
}
