<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->passwordEncoder = $userPasswordEncoder;
    }
    public function load(ObjectManager $manager)
    {
        
        $plainPassword = '123456';
        //create normal user 
        for ($i = 0; $i < 20; $i++) {

            $user = new User();
            $user->setEmail('user_'.$i.'@gmail.com');
            $user->setFirstName('Name '.$i.'');
            $user->setLastname('LastName '.$i.'');
            $user->setPlainPassword($plainPassword);
            $password = $this->passwordEncoder->encodePassword($user, $plainPassword);
            $user->setPassword($password);
            $manager->persist($user);
        }

        //admin user 
        $user = new User();
        $user->setEmail('Admin@gmail.com');
        $user->setFirstName('Admin Name');
        $user->setLastname('Admin LastName');
        $user->setPlainPassword($plainPassword);
        $password = $this->passwordEncoder->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        $roles = $user->getRoles();
        $roles[] = 'ROLE_ADMIN';
        $user->setRoles($roles);

        $manager->persist($user);
        

        $manager->flush();
    }
}
