<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\VerificationRequest;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class AddOwnerToVerificationRequestSubscriber implements EventSubscriberInterface
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {

        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {

        return [
            KernelEvents::VIEW => ['attachOwnerToVerificationRequest', EventPriorities::PRE_WRITE],
        ];
    }

    public function attachOwnerToVerificationRequest(ViewEvent $event)
    {
 
        $vr = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$vr instanceof VerificationRequest || Request::METHOD_POST !== $method) {
            return;
        }
 
        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return;
        }

        $owner = $token->getUser();

        if (!$owner instanceof User) {
            return;
        }

        // Attach the user to the not yet persisted Post
        $vr->setUser($owner);

    }
}