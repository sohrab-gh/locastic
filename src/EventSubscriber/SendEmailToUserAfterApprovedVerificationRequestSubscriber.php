<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\VerificationRequest;
use Swift_Message;
use Swift;


class SendEmailToUserAfterApprovedVerificationRequestSubscriber implements EventSubscriberInterface
{

    private $tokenStorage;
    private $mailer;

    public function __construct(TokenStorageInterface $tokenStorage, \Swift_Mailer $mailer)
    {
        $this->tokenStorage = $tokenStorage;
        $this->mailer = $mailer;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendEmailToApprovedUser', EventPriorities::POST_WRITE],
        ];
    }
    public function sendEmailToApprovedUser(ViewEvent $event)
    {
        $vr = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        $request = $event->getRequest();
       
        if (!$vr instanceof VerificationRequest || Request::METHOD_PATCH !== $method) {
            return;
        }

        if($request->attributes->get('_api_item_operation_name')!='approve_request'){
            return;
        }

        $token = $this->tokenStorage->getToken();
        $currentUser = $token->getUser();

        if (!in_array('ROLE_ADMIN', $currentUser->getRoles())) {
            return;
        }

        if($vr->getStatus() == VerificationRequest::STATUS_VERIFICATION_APPROVED){
            $message = (new \Swift_Message('Verification Approval'))
            ->setFrom('someOne@someWhere.com')
            ->setTo($vr->getUser()->getEmail())
            ->setBody("Your Verification Request Has Been Approved. You Can Now Post In Blog");
            
            $this->mailer->send($message);
        }
        return;


    }
}
