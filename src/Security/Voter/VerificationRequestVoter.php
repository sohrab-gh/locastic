<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntitySerializer;
use App\Entity\VerificationRequest;

class VerificationRequestVoter extends Voter
{
    private $security = null;
    private $entityManager = null;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    protected function supports($attribute, $subject)
    {

        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['VERIFICATION_REQUESEST_EDIT'])
            && $subject instanceof \App\Entity\VerificationRequest;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }
        $vr = $this->entityManager->getRepository(VerificationRequest::class)->findBy(array('user'=>$user));
        if($vr){
            $vr = $vr->first();
        }
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'VERIFICATION_REQUESEST_EDIT':
                if (in_array('ROLE_ADMIN', $user->getRoles())) {
                    return true;
                }
                else{
                    if($vr){
                        if($vr->getStatus()== VerificationRequest::STATUS_VERIFICATION_REQUESTED){
                            return true;
                        }
                        else{
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }
                break;
        }

        return false;
    }
}
