<?php

namespace App\Security\Voter;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntitySerializer;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\VerificationRequest;

class PostVoter extends Voter
{
    private $security = null;
    private $entityManager = null;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }
    protected function supports($attribute, $subject)
    {    
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['POST_CREATE', 'POST_READ', 'POST_EDIT', 'POST_DELETE'])
            && $subject instanceof \App\Entity\Post;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

     
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        $vr = $this->entityManager->getRepository(VerificationRequest::class)->findBy(array('user'=>$user));
        if($vr){
            $vr = $vr->first();
        }
        
        switch ($attribute) {
            case 'POST_CREATE':
                if($vr){
                    if($vr->getStatus()== VerificationRequest::STATUS_VERIFICATION_APPROVED){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            case 'POST_EDIT':
                if($vr){
                    if($vr->getStatus()== VerificationRequest::STATUS_VERIFICATION_APPROVED){
                        if($subject->getUser() == $user){
                            return true;
                        }
                        else{
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
                break;
            case 'POST_READ':
                return true;
                break;
            case 'POST_DELETE':
                return false;
                break;
        }

        return false;
    }
}
