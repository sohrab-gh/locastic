<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *  routePrefix="/blog",
 *  collectionOperations={
 *         "get",
 *         "post" = { "security_post_denormalize" = "is_granted('POST_CREATE', object)","security_post_denormalize_message"="Only verified users can post" }
 *  },
 *  itemOperations={
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *      "put"={"security"="is_granted('ROLE_ADMIN') or is_granted('POST_EDIT', object) "},
 *      "patch" ={"security"="is_granted('ROLE_ADMIN') or is_granted('POST_EDIT', object) "},
 *      "get"
 *  },
 *  normalizationContext={"groups"={"post:read"}},
 *  denormalizationContext={"groups"={"post:write"}}
 * )
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string The title of post.
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     * @Groups({"post:read", "post:write"})
     */
    public $title;

    /**
     * @var string The content of the post.
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Groups({"post:read", "post:write"})
     */
    public $content;

    /**
     * @var \DateTimeInterface Date of the post
     * @ORM\Column(name="create_date")
     * @Assert\NotNull
     * @Groups({"post:read"})
     */
    public $createDate;

    /**
     * One user has many posts
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @Groups({"post:read"})
     */
    public $author;
    

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setUser(User $author): self
    {
        $this->author = $author;
        return $this;
    }
    public function getUser(): ?User
    {
        return $this->user;
    }
    public function __construct(){
        $datetime = new \DateTime();
        $this->createDate = $datetime->format('Y-m-d H:i:s');
    }
}
