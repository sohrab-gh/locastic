<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;


/**
 * @ApiResource(
 *  collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"},
 *  },
 *  itemOperations={
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *      "put"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object == user) "},
 *      "patch" ={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"post:change_role"}},
 *      },
 *      "get"
 *  },
 *   normalizationContext={"groups"={"post:read"}},
 *   denormalizationContext={"groups"={"post:write"}}
 * 
 * )
 * @Orm\Table("`user`")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *  @var string user email
     *  * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
     *     message="not_valid_email"
     * )
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank
     * @Groups({"post:read", "post:write"})
     */
    private $email;


    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(name="plain_password")
     * @ORM\Column(type="string",length=20)
     * @Assert\NotNull
     * @Groups({"post:write"})
     */
    private $plainPassword;


    /**
     * @var string The user first name 
     * @ORM\Column(type="string",length=20)
     * @Assert\NotBlank
     * @Groups({"post:read", "post:write"})
     */
    private $firstName;

    /**
     * @var string The user last name 
     * @ORM\Column(type="string",length=20)
     * @Assert\NotBlank
     * @Groups({"post:read", "post:write"})
     */
    private $lastName;


    /**
     * @var \DateTimeInterface Date of the post
     * @ORM\Column(name="create_date")
     * @ORM\Column(type="datetime")
     * @Assert\NotNull
     * @Groups({"post:read"})
     */
    public $createDate;


    /**
     * @var array The user roles json array 
     * @ORM\Column(type="json")
     * @Groups({"get"})
     * @Groups({"post:read","post:change_role"})
     */
    private $roles = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }
    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastName;
    }
    public function setLastname(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
    
    public function __construct(){
        $this->roles = $this->getRoles();
        $datetime = new \DateTime();
        $this->createDate = $datetime->format('Y-m-d H:i:s');
    }
}
