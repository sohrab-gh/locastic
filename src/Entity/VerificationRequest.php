<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Repository\VerificationRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\ApproveVerificationRequestController;
use App\Controller\RejectVerificationRequestController;

/**
 * @ApiResource(
 *  routePrefix="/register",
 *  collectionOperations={
 *         "get" = { "security" = "is_granted('ROLE_ADMIN')"},
 *          "post" = {
 *                      "security_post_denormalize" = "is_granted('ROLE_USER')",
 *                      "denormalization_context"={"groups"={"verificationrequest:write"}},
 *          },
 *  },
 *  itemOperations={
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *      "put"={"security"="is_granted('ROLE_ADMIN') or is_granted('VERIFICATION_REQUESEST_EDIT', object) "},
 *      "patch" ={"security"="is_granted('ROLE_ADMIN') or is_granted('VERIFICATION_REQUESEST_EDIT', object) "},
 *      "get",
 *      "approve_request"={
 *              "method"="PATCH",
 *              "path"="/verification_requests/{id}/approve_request",
 *              "controller"=ApproveVerificationRequestController::class,
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"verificationrequest:vr"}},
 *       },
 *       "reject_request"={
 *              "method"="PATCH",
 *              "path"="/reject_request/{id}/reject_request",
 *              "controller"=RejectVerificationRequestController::class,
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"verificationrequest:reject_request"}},
 *       }
 *  },
 *  normalizationContext={"groups"={"verificationrequest:read"}},
 *  denormalizationContext={"groups"={"verificationrequest:write","verificationrequest:ar"}}
 * )
 * @ApiFilter(OrderFilter::class, properties={"createDate"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"user": "exact", "status": "exact"})
 * @ORM\Entity(repositoryClass=VerificationRequestRepository::class)
 */
class VerificationRequest
{

    const STATUS_VERIFICATION_REQUESTED = 1;
    const STATUS_VERIFICATION_APPROVED = 2;
    const STATUS_VERIFICATION_DECLINED = 3;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var MediaObject|null
     *
     * @ORM\OneToOne(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @ApiProperty(iri="http://schema.org/image")
     * @Groups({"verificationrequest:read", "verificationrequest:write"})
     */
    public $image;

    /**
     * @var string verification request optional message.
     * @ORM\Column(type="text")
     * @Groups({"verificationrequest:read", "verificationrequest:write"})
     */
    public $message;

    /**
     * @var string verification request rejection message.
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"verificationrequest:read", "verificationrequest:reject_request"})
     */
    public $rejectionMessage;

    /**
     * @var \DateTimeInterface Date of the request
     * @ORM\Column(name="create_date")
     * @ORM\Column(type="datetime")
     * @Groups({"verificationrequest:read"})
     * @Assert\NotNull
     */
    public $createDate;

    /**
     * status of the verification request
     * @ORM\Column(type="smallint")
     * @Assert\NotNull
     * @Groups({"verificationrequest:read"})
     */
    public $status;

    /**
     * one to one relation with user
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",nullable=false)
     * @Groups({"verificationrequest:read"})
     */
    public $user;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getStatus(): ?int
    {
        return $this->status;
    }
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }
    public function getUser(): ?User
    {
        return $this->user;
    }

    public function __construct()
    {
        $datetime = new \DateTime();
        $this->createDate = $datetime->format('Y-m-d H:i:s');
        $this->status = self::STATUS_VERIFICATION_REQUESTED;
    }
}
