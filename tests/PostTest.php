<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use App\Entity\Post;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpClient\HttpClient;

class PostTest extends ApiTestCase
{

    const USERNAME = 'Admin@gmail.com';
    const PASSWORD = '123456';

    public function testGetPosts(): void
    {
        $response = static::createClient()->request('GET', '/blog/posts');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $items = [
            
            "@context"=> "/contexts/Post",
            "@id"=>  "/blog/posts",
            "@type"=>  "hydra:Collection",
        ];
        $this->assertJsonContains($items);
        $this->assertMatchesResourceCollectionJsonSchema(Post::class);
        
    }
    public function testCreatePost(): void
    {

        $client = static::createClient();
        
        $token = $this->login();
        
        $random = rand(1,1000);
        $randomTitle = sprintf('title %d', $random);
        $randomContent = sprintf('title %d', $random);

        $response = $client->request('POST', '/blog/posts', [
            'headers'=>[
                'Authorization'=>"Bearer ".$token
            ],
            'json' => [
                'title' => $randomTitle,
                'content' => $randomContent,
            ]
        ]
        );

        $this->assertResponseStatusCodeSame(403);
        
 
    }

    public function login(): string
    {
        $response = static::createClient()->request('POST', '/authentication_token', ['json' => [
            'username' => PostTest::USERNAME,
            'password' => PostTest::PASSWORD,
        ]]);
        return $response->toArray()['token'];
    }


}
