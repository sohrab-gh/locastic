<?php


namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;


class UserTest extends ApiTestCase
{
    //use RefreshDatabaseTrait;

    public function testGetUsers(): void
    {
        $response = static::createClient()->request('GET', '/users');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $items = [
            
            "@context"=> "/contexts/User",
            "@id"=>  "/users",
            "@type"=>  "hydra:Collection",
        ];
        $this->assertJsonContains($items);
        $this->assertMatchesResourceCollectionJsonSchema(User::class);
        
    }
    
    public function testCreateUser(): void
    {
        $randomUserName = 'test_user'.rand(1,2000).'@gmail.com';
        $response = static::createClient()->request('POST', '/users', ['json' => [
            'email' => $randomUserName,
            'plainPassword' => '123456',
            'firstName' => 'Test User',
            'lastName' => 'Test User Lastname',
        ]]);

        
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            '@context' => '/contexts/User',
            '@type' => 'User',
            'email' => $randomUserName,
            'firstName' => 'Test User',
            'lastName' => 'Test User Lastname'
        ]);
        $this->assertRegExp('~^/users/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(User::class);
    }
    

    public function testLoginUnSuccessful(): void
    {
        $response = static::createClient()->request('POST', '/authentication_token', ['json' => [
            'username' => 'x@example.com',
            'password' => '1234567',
        ]]);

        $this->assertResponseStatusCodeSame(401);
        $items = [
            "code"=>401,
            "message"=> "Invalid credentials."
        ];
        $this->assertJsonContains($items);
    }
    
    
    public function testLoginSuccessful(): void
    {
        $response = static::createClient()->request('POST', '/authentication_token', ['json' => [
            'username' => 'user_0@gmail.com',
            'password' => '123456',
        ]]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseIsSuccessful();
        //return $response->toArray()['token'];
    }
    
}
